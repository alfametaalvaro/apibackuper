﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api_backuper
{
    class AgendaBackup
    {
        public AgendaBackup ()
        {
               
        }

        public void CriaAgenda(Agendamento agendamento)
        {
            Process proc = new Process();
            proc.StartInfo.FileName = "schtasks.exe";
            proc.StartInfo.Arguments = "/create /tn APIBackuperAgenda /tr \"" + AppDomain.CurrentDomain.BaseDirectory + "Api_backuper.exe -a\""   
                + " /sc DAILY /st " + agendamento.hora + " /ru " + agendamento.usuario + " /rp " + agendamento.senha + " /rl highest";
            //
            proc.StartInfo.CreateNoWindow = true;
            proc.StartInfo.Verb = "runas";
            proc.Start();
            proc.WaitForExit();
            proc.Close();
        }
        
        public void DeletaAgenda()
        {
            Process proc = new Process();
            proc.StartInfo.FileName = "schtasks.exe";
            proc.StartInfo.Arguments = "/delete /tn APIBackuperAgenda /f";
            proc.StartInfo.Verb = "runas";
            proc.StartInfo.CreateNoWindow = true;
            proc.Start();
            proc.WaitForExit();
            proc.Close();
        }

    }
}



/*public void CriaAgenda(Agendamento agendamento)
        {
            configuracoes = configurador.LeArquivoConfiguracaoServidor();
            parametrosBackupP = "--host " + configuracoes.IpHost + " --port " + configuracoes.Porta + " -username " + configuracoes.UserBanco + " --format p --file " + caminhoBackupBase + "\\baseP.backup " + configuracoes.NomeBase;
           
            Process proc = new Process();
            proc.StartInfo.FileName = "schtasks.exe";
            proc.StartInfo.Arguments = "/create /tn APIBackuperP /tr "+ configuracoes.CaminhoPostgre + "\\pg_dump.exe " + 
                parametrosBackupP + " /sc DAILY /st " + agendamento.hora + " /u " + agendamento.usuario + " /p " + agendamento.senha;
           
proc.StartInfo.CreateNoWindow = true;
            proc.Start();
            proc.WaitForExit();
            proc.Close();

            parametrosBackupC = "--host " + configuracoes.IpHost + " --port " + configuracoes.Porta + " -username " + configuracoes.UserBanco + " --format c --file " + caminhoBackupBase + "\\baseP.backup " + configuracoes.NomeBase;

            proc.StartInfo.Arguments = "/create /tn APIBackuperC /tr " + configuracoes.CaminhoPostgre + "\\pg_dump.exe " +
            parametrosBackupC + " /sc DAILY /st " + agendamento.hora + " /u .\\" + agendamento.usuario + " /p " + agendamento.senha;
            proc.StartInfo.CreateNoWindow = true;
            proc.Start();
            proc.WaitForExit();
            proc.Close();

            parametrosBackupT = "--host " + configuracoes.IpHost + " --port " + configuracoes.Porta + " -username " + configuracoes.UserBanco + " --format t --file " + caminhoBackupBase + "\\baseP.backup " + configuracoes.NomeBase;

            proc.StartInfo.Arguments = "/create /tn APIBackuperT /tr " + configuracoes.CaminhoPostgre + "\\pg_dump.exe " +
            parametrosBackupT + " /sc DAILY /st " + agendamento.hora + " /u " + agendamento.usuario + " /p " + agendamento.senha;
            proc.StartInfo.CreateNoWindow = true;
            proc.Start();
            proc.WaitForExit();
            proc.Close();

        }*/
