﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Api_backuper
{
    public partial class FormAgendaBackup : Form
    {
        Agendamento agendamento;
        AgendaBackup agenda;
        public FormAgendaBackup()
        {
            InitializeComponent();
            agendamento = new Agendamento();
            agenda = new AgendaBackup();
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void TextBoxHora_TextChanged(object sender, EventArgs e)
        {

        }

        private void ButtonSalvar_Click(object sender, EventArgs e)
        {
            agendamento.usuario = textBoxUserAdm.Text;
            agendamento.senha = textBoxSenhaAdm.Text;
            agendamento.hora = textBoxHora.Text;
            if (checkBoxAtivar.Checked)
            {
                agenda.DeletaAgenda();
                agenda.CriaAgenda(agendamento);
            }
            else
            {
                agenda.DeletaAgenda();
            }
            this.Close();
        }

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void ButtonCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
