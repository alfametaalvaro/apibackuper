﻿using IniParser;
using IniParser.Model;
using System;
using System.IO;
using System.Windows.Forms;
namespace Api_backuper {
    public class Configurador {
        ConfiguracaoModel configuracoes;
        IniData data;
        string caminhoBackuper = AppDomain.CurrentDomain.BaseDirectory + "BackupCfg.ini";
        public Configurador() {
            
            var parser = new FileIniDataParser();
            if (File.Exists(caminhoBackuper)) {
                configuracoes = new ConfiguracaoModel();
                data = parser.ReadFile(caminhoBackuper);
                LeArquivoConfiguracao();
            }
            else {
                //MessageBox.Show("Backup ainda não configurado");
                data = new IniData();
            }
        }
        public ConfiguracaoModel LeArquivoConfiguracao() {
            if(configuracoes == null) {
                return null;
            }
            else {
                configuracoes.PastaApi = data["Configuracoes"]["PastaApi"];
                configuracoes.PastaSite = data["Configuracoes"]["PastaSite"];
                configuracoes.PastaDestino = data["Configuracoes"]["PastaDestino"];
                return configuracoes;
            }
        }

        public void SalvaConfiguracoes(ConfiguracaoModel _configuracoes) {
            data["Configuracoes"]["PastaApi"] =_configuracoes.PastaApi;
            data["Configuracoes"]["PastaSite"] = _configuracoes.PastaSite;
            data["Configuracoes"]["PastaDestino"] = _configuracoes.PastaDestino;
            var parser = new FileIniDataParser();
            parser.WriteFile("BackupCfg.ini", data);
        }

        

        public ConfiguracaoModel LeArquivoConfiguracaoServidor()
        {
            if (configuracoes == null)
            {
                return null;
            }
            else
            {
                configuracoes.NomeBase = data["ConfiguracoesServidor"]["NomeBase"];
                configuracoes.CaminhoPostgre = data["ConfiguracoesServidor"]["CaminhoPostgre"];
                configuracoes.SenhaBanco = data["ConfiguracoesServidor"]["SenhaBanco"];
                configuracoes.UserBanco = data["ConfiguracoesServidor"]["UserBanco"];
                configuracoes.IpHost = data["ConfiguracoesServidor"]["IpHost"];
                configuracoes.Porta = data["ConfiguracoesServidor"]["Porta"];

                return configuracoes;
            }
        }


        public void SalvaConfiguracoesServidor(ConfiguracaoModel _configuracoes)
        {
            data["ConfiguracoesServidor"]["NomeBase"] = _configuracoes.NomeBase;
            data["ConfiguracoesServidor"]["CaminhoPostgre"] = _configuracoes.CaminhoPostgre;
            data["ConfiguracoesServidor"]["SenhaBanco"] = _configuracoes.SenhaBanco;
            data["ConfiguracoesServidor"]["UserBanco"] = _configuracoes.UserBanco;
            data["ConfiguracoesServidor"]["IpHost"] = _configuracoes.IpHost;
            data["ConfiguracoesServidor"]["Porta"] = _configuracoes.Porta;
            var parser = new FileIniDataParser();
            if (File.Exists(caminhoBackuper))
            { 
                parser.WriteFile("BackupCfg.ini", data);
            }
            else
            {
                File.Create(caminhoBackuper);
                
                try
                {
                 parser.WriteFile("BackupCfg.ini", data);
                }catch(Exception e)
                {
                    MessageBox.Show("O Arquivo não pôde ser acessado, favor realizar o salvamento novamente.");
                }
            }
        }

       
    }
}
