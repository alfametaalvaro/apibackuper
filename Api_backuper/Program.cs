﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Api_backuper {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args) {
            if (args.Length == 0) { 
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());
            }else if (args.Contains("-a"))
            {
                ConfiguracaoModel configuracoes;
                Configurador configurador = new Configurador();
                configuracoes = configurador.LeArquivoConfiguracao();
                configuracoes = configurador.LeArquivoConfiguracaoServidor();
                FazBackup backup = new FazBackup(configuracoes);
                backup.IniciaBackupSilencioso();                
                Application.Exit();
            }
        }
    }
}
