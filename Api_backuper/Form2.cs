﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Api_backuper
{
    public partial class ConfigBase : Form
    {
        public Form1 form1 { get; set; }
        ConfiguracaoModel configuracoes;
        Configurador configurador;
        public ConfigBase()
        {
            InitializeComponent();
            configurador = new Configurador();
            configuracoes = new ConfiguracaoModel();            
            configuracoes = configurador.LeArquivoConfiguracaoServidor();
            if(configuracoes == null)
            {
                configuracoes = new ConfiguracaoModel();
            }
            CarregaCampos();
        }

        private void BtnDestinoCopia_Click(object sender, EventArgs e)
        {
            using (var ArquivoDestino = new OpenFileDialog())
            {
                DialogResult result = ArquivoDestino.ShowDialog();
                if (result == DialogResult.OK)
                {
                    txtCaminhoPostgre.Text = ArquivoDestino.FileName;

                }
            }
        }

       
        private void ButtonSalvar_Click(object sender, EventArgs e)
        {
            
            if (string.IsNullOrEmpty(textBoxBase.Text))
            {
                MessageBox.Show("Nome da Base não informado!");
                return;
            } else configuracoes.NomeBase = textBoxBase.Text;
            if (string.IsNullOrEmpty(txtCaminhoPostgre.Text))
            {
                MessageBox.Show("Caminho do Postgre não informado!");
                return;
            } else configuracoes.CaminhoPostgre = txtCaminhoPostgre.Text;

            if (string.IsNullOrEmpty(textBoxIP.Text))
            {
                MessageBox.Show("IP não informado!");
                return;
            }
            else configuracoes.IpHost = textBoxIP.Text;
            if (string.IsNullOrEmpty(textBoxPorta.Text))
            {
                MessageBox.Show("Porta da Base não informado!");
                return;
            }
            else configuracoes.Porta = textBoxPorta.Text;
            if (string.IsNullOrEmpty(textBoxSenha.Text))
            {
                MessageBox.Show("Senha da Base não informado!");
                return;
            }
            else configuracoes.SenhaBanco = textBoxSenha.Text;
            if (string.IsNullOrEmpty(textBoxUsuario.Text))
            {
                MessageBox.Show("Usuario da Base não informado!");
                return;
            }
            else configuracoes.UserBanco = textBoxUsuario.Text;
            configurador.SalvaConfiguracoesServidor(configuracoes);
            form1.Configura(txtCaminhoPostgre.Text, textBoxIP.Text, textBoxPorta.Text, textBoxSenha.Text, textBoxUsuario.Text, textBoxBase.Text);
            this.Close();
            
        }

        private void ButtonCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        

        private void BtnPastaApi_Click(object sender, EventArgs e)
        {
            using (var pastaPostgre = new FolderBrowserDialog())
            {
                DialogResult result = pastaPostgre.ShowDialog();
                if (result == DialogResult.OK)
                {
                  txtCaminhoPostgre.Text = pastaPostgre.SelectedPath;
                }
            }
        }

        private void CarregaCampos()
        {
            if (configuracoes != null)
            {
                textBoxBase.Text = configuracoes.NomeBase;
                txtCaminhoPostgre.Text = configuracoes.CaminhoPostgre;
                textBoxIP.Text = configuracoes.IpHost;
                textBoxPorta.Text = configuracoes.Porta;
                textBoxSenha.Text = configuracoes.SenhaBanco;
                textBoxUsuario.Text = configuracoes.UserBanco;
            }
        }
    }

}
