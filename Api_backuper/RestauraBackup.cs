﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Windows.Forms;
using System.Diagnostics;

namespace Api_backuper
{
    class RestauraBackup
    {
        ConfiguracaoModel configuracoes;
        Form1 form;
        string parametros;
        string backup;

        public RestauraBackup(ConfiguracaoModel _configuracoes, Form1 _form)
        {
            configuracoes = _configuracoes;
            form = _form;
            parametros = "-h " +configuracoes.IpHost +" -p "+configuracoes.Porta + " -U "+ configuracoes.UserBanco + " --dbname=" + configuracoes.NomeBase + " --create --verbose ";
            
        }

        
        public async void Restaurar()
        {
            MessageBox.Show("Antes de prosseguir com a restauração do Backup," +
                "certifique-se de que já exista uma base de mesmo nome criada no Banco de Dados.");
            form.MensagemRetorno = "\r\n Restauração Iniciada... \r\n";
            form.AtualizaSaida();

            System.IO.DirectoryInfo di = new DirectoryInfo(configuracoes.PastaDestino + "\\temp");
            if (di.Exists)
            {
                try
                {
                    foreach (FileInfo file in di.GetFiles())
                    {
                        file.Delete();
                    }
                    foreach (DirectoryInfo dir in di.GetDirectories())
                    {
                        dir.Delete(true);
                    }
                    Directory.Delete(configuracoes.PastaDestino + "\\temp");
                    form.MensagemRetorno = "\r\n Restauração Concluída com Sucesso! \r\n";
                    form.AtualizaSaida();
                }
                catch (Exception e)
                {
                    form.MensagemRetorno = "\r\n Erro ao Deletar arquivos temporários antes da restauração. \r\n";
                    form.AtualizaSaida();
                }
            }
            if (Extrai())
            {
                Process proc = new Process();
                Environment.SetEnvironmentVariable("PGPASSWORD", configuracoes.SenhaBanco);
                proc.StartInfo.FileName = configuracoes.CaminhoPostgre + "\\pg_restore.exe";
                proc.StartInfo.Arguments = parametros + "\"" + configuracoes.PastaDestino + "\\temp\\BASE\\baset.backup\"";
                proc.StartInfo.CreateNoWindow = false;
                proc.Start();
                proc.WaitForExit();
                proc.Close();
                //di = new DirectoryInfo(configuracoes.PastaDestino + "\\temp");
                try
                {
                    foreach (FileInfo file in di.GetFiles())
                    {
                        file.Delete();
                    }
                    foreach (DirectoryInfo dir in di.GetDirectories())
                    {
                        dir.Delete(true);
                    }
                    Directory.Delete(configuracoes.PastaDestino + "\\temp");
                    form.MensagemRetorno = "\r\n Restauração Concluída com Sucesso! \r\n";
                    form.AtualizaSaida();
                }catch (Exception e)
                {
                    form.MensagemRetorno = "\r\n Erro ao Deletar arquivos temporários após restauração. \r\n";
                    form.AtualizaSaida();
                }
            }
            else
            {
                return;
            }
        }

        public Boolean Extrai()
        {
            using (var arquivoBackup = new OpenFileDialog())
            {
                arquivoBackup.InitialDirectory = configuracoes.PastaDestino;
                DialogResult result = arquivoBackup.ShowDialog();
                try
                {
                    if (result == DialogResult.OK)
                    {
                        backup = Path.GetFullPath(arquivoBackup.FileName);
                        ZipFile.ExtractToDirectory(backup, configuracoes.PastaDestino + "\\temp");
                        return true;

                    }
                    else
                    {
                        return false;
                    }
                }catch(Exception e)
                {
                    form.MensagemRetorno = "\r\n Erro durante a extração dos arquivos\r\n";
                    form.AtualizaSaida();
                    return false;
                }
            }
        }
        }
                
                
}

    

