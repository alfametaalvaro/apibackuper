﻿using System;

namespace Api_backuper
{
    partial class ConfigBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxIP = new System.Windows.Forms.TextBox();
            this.labelIP = new System.Windows.Forms.Label();
            this.labelPorta = new System.Windows.Forms.Label();
            this.textBoxPorta = new System.Windows.Forms.TextBox();
            this.labelUsuario = new System.Windows.Forms.Label();
            this.textBoxUsuario = new System.Windows.Forms.TextBox();
            this.labelSenha = new System.Windows.Forms.Label();
            this.textBoxSenha = new System.Windows.Forms.TextBox();
            this.labelNomeBase = new System.Windows.Forms.Label();
            this.textBoxBase = new System.Windows.Forms.TextBox();
            this.txtCaminhoPostgre = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonSalvar = new System.Windows.Forms.Button();
            this.buttonCancelar = new System.Windows.Forms.Button();
            this.btnPastaPostgre = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxIP
            // 
            this.textBoxIP.Location = new System.Drawing.Point(177, 25);
            this.textBoxIP.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxIP.Name = "textBoxIP";
            this.textBoxIP.Size = new System.Drawing.Size(235, 22);
            this.textBoxIP.TabIndex = 0;
            //this.textBoxIP.TextChanged += new System.EventHandler(this.TextBoxIP_TextChanged);
            // 
            // labelIP
            // 
            this.labelIP.AutoSize = true;
            this.labelIP.Location = new System.Drawing.Point(24, 28);
            this.labelIP.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelIP.Name = "labelIP";
            this.labelIP.Size = new System.Drawing.Size(144, 16);
            this.labelIP.TabIndex = 1;
            this.labelIP.Text = "IP do Banco de Dados";
            //this.labelIP.Click += new System.EventHandler(this.Label1_Click);
            // 
            // labelPorta
            // 
            this.labelPorta.AutoSize = true;
            this.labelPorta.Location = new System.Drawing.Point(129, 62);
            this.labelPorta.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPorta.Name = "labelPorta";
            this.labelPorta.Size = new System.Drawing.Size(40, 16);
            this.labelPorta.TabIndex = 3;
            this.labelPorta.Text = "Porta";
            //this.labelPorta.Click += new System.EventHandler(this.Label1_Click_1);
            // 
            // textBoxPorta
            // 
            this.textBoxPorta.Location = new System.Drawing.Point(177, 59);
            this.textBoxPorta.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPorta.Name = "textBoxPorta";
            this.textBoxPorta.Size = new System.Drawing.Size(93, 22);
            this.textBoxPorta.TabIndex = 2;
            //this.textBoxPorta.TextChanged += new System.EventHandler(this.TextBox1_TextChanged);
            // 
            // labelUsuario
            // 
            this.labelUsuario.AutoSize = true;
            this.labelUsuario.Location = new System.Drawing.Point(114, 93);
            this.labelUsuario.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUsuario.Name = "labelUsuario";
            this.labelUsuario.Size = new System.Drawing.Size(55, 16);
            this.labelUsuario.TabIndex = 5;
            this.labelUsuario.Text = "Usuário";
            // 
            // textBoxUsuario
            // 
            this.textBoxUsuario.Location = new System.Drawing.Point(177, 93);
            this.textBoxUsuario.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxUsuario.Name = "textBoxUsuario";
            this.textBoxUsuario.Size = new System.Drawing.Size(235, 22);
            this.textBoxUsuario.TabIndex = 4;
            // 
            // labelSenha
            // 
            this.labelSenha.AutoSize = true;
            this.labelSenha.Location = new System.Drawing.Point(121, 126);
            this.labelSenha.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelSenha.Name = "labelSenha";
            this.labelSenha.Size = new System.Drawing.Size(47, 16);
            this.labelSenha.TabIndex = 6;
            this.labelSenha.Text = "Senha";
            // 
            // textBoxSenha
            // 
            this.textBoxSenha.Location = new System.Drawing.Point(176, 123);
            this.textBoxSenha.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxSenha.Name = "textBoxSenha";
            this.textBoxSenha.PasswordChar = '*';
            this.textBoxSenha.Size = new System.Drawing.Size(235, 22);
            this.textBoxSenha.TabIndex = 7;
            // 
            // labelNomeBase
            // 
            this.labelNomeBase.AutoSize = true;
            this.labelNomeBase.Location = new System.Drawing.Point(70, 161);
            this.labelNomeBase.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNomeBase.Name = "labelNomeBase";
            this.labelNomeBase.Size = new System.Drawing.Size(99, 16);
            this.labelNomeBase.TabIndex = 9;
            this.labelNomeBase.Text = "Nome da Base";
           // this.labelNomeBase.Click += new System.EventHandler(this.Label1_Click_2);
            // 
            // textBoxBase
            // 
            this.textBoxBase.Location = new System.Drawing.Point(177, 158);
            this.textBoxBase.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxBase.Name = "textBoxBase";
            this.textBoxBase.Size = new System.Drawing.Size(235, 22);
            this.textBoxBase.TabIndex = 8;
            //this.textBoxBase.TextChanged += new System.EventHandler(this.TextBox2_TextChanged);
            // 
            // txtCaminhoPostgre
            // 
            this.txtCaminhoPostgre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCaminhoPostgre.Location = new System.Drawing.Point(176, 188);
            this.txtCaminhoPostgre.Name = "txtCaminhoPostgre";
            this.txtCaminhoPostgre.Size = new System.Drawing.Size(236, 22);
            this.txtCaminhoPostgre.TabIndex = 19;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(26, 191);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(143, 16);
            this.label6.TabIndex = 18;
            this.label6.Text = "Pasta \"bin\" do Postgre";
            //this.label6.Click += new System.EventHandler(this.Label6_Click);
            // 
            // buttonSalvar
            // 
            this.buttonSalvar.Location = new System.Drawing.Point(166, 229);
            this.buttonSalvar.Name = "buttonSalvar";
            this.buttonSalvar.Size = new System.Drawing.Size(72, 28);
            this.buttonSalvar.TabIndex = 21;
            this.buttonSalvar.Text = "Salvar";
            this.buttonSalvar.UseVisualStyleBackColor = true;
            this.buttonSalvar.Click += new System.EventHandler(this.ButtonSalvar_Click);
            // 
            // buttonCancelar
            // 
            this.buttonCancelar.Location = new System.Drawing.Point(278, 229);
            this.buttonCancelar.Name = "buttonCancelar";
            this.buttonCancelar.Size = new System.Drawing.Size(72, 28);
            this.buttonCancelar.TabIndex = 22;
            this.buttonCancelar.Text = "Cancelar";
            this.buttonCancelar.UseVisualStyleBackColor = true;
            this.buttonCancelar.Click += new System.EventHandler(this.ButtonCancelar_Click);
            // 
            // btnPastaPostgre
            // 
            this.btnPastaPostgre.Image = global::Api_backuper.Properties.Resources.icons8_live_folder_16;
            this.btnPastaPostgre.Location = new System.Drawing.Point(417, 187);
            this.btnPastaPostgre.Name = "btnPastaPostgre";
            this.btnPastaPostgre.Size = new System.Drawing.Size(28, 23);
            this.btnPastaPostgre.TabIndex = 23;
            this.btnPastaPostgre.UseVisualStyleBackColor = true;
            this.btnPastaPostgre.Click += new System.EventHandler(this.BtnPastaApi_Click);
            // 
            // ConfigBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 285);
            this.Controls.Add(this.btnPastaPostgre);
            this.Controls.Add(this.buttonCancelar);
            this.Controls.Add(this.buttonSalvar);
            this.Controls.Add(this.txtCaminhoPostgre);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.labelNomeBase);
            this.Controls.Add(this.textBoxBase);
            this.Controls.Add(this.textBoxSenha);
            this.Controls.Add(this.labelSenha);
            this.Controls.Add(this.labelUsuario);
            this.Controls.Add(this.textBoxUsuario);
            this.Controls.Add(this.labelPorta);
            this.Controls.Add(this.textBoxPorta);
            this.Controls.Add(this.labelIP);
            this.Controls.Add(this.textBoxIP);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfigBase";
            this.Text = "Configurar Base de Dados";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        
        

        #endregion

        private System.Windows.Forms.TextBox textBoxIP;
        private System.Windows.Forms.Label labelIP;
        private System.Windows.Forms.Label labelPorta;
        private System.Windows.Forms.TextBox textBoxPorta;
        private System.Windows.Forms.Label labelUsuario;
        private System.Windows.Forms.TextBox textBoxUsuario;
        private System.Windows.Forms.Label labelSenha;
        private System.Windows.Forms.TextBox textBoxSenha;
        private System.Windows.Forms.Label labelNomeBase;
        private System.Windows.Forms.TextBox textBoxBase;
        private System.Windows.Forms.TextBox txtCaminhoPostgre;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonSalvar;
        private System.Windows.Forms.Button buttonCancelar;
        private System.Windows.Forms.Button btnPastaPostgre;
    }
}