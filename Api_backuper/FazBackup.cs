﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Api_backuper {
    public class FazBackup {
        ConfiguracaoModel configuracoes;
        //string caminhoBackupBase = @"C:\\baseBackup\\";
        string parametrosBackup;
        Form1 form;
        //public Boolean status { get; set; }
        public FazBackup(ConfiguracaoModel _configuracoes, Form1 _form){
            configuracoes = _configuracoes;
            form = _form;            
        }
        public FazBackup(ConfiguracaoModel _configuracoes)
        {
            configuracoes = _configuracoes;
            //status = false;
            
        }

        public async Task IniciaBackup() {
            if (Directory.Exists(configuracoes.PastaTemporaria))
            {
                try
                {
                    LimpaTemp();
                } catch (Exception erroLimpeza)
                {
                    form.MensagemRetorno = "Falha ao excluir arquivos temporários";
                    form.AtualizaSaida();
                }
            }
            if(configuracoes.PastaDestino == null || !Directory.Exists(configuracoes.PastaDestino))
            {
                MessageBox.Show("Pasta de Destino não configurada ou inexistente.");
                form.FinalizaBackup();
                return;
            }
            if (configuracoes.PastaApi != null && Directory.Exists(configuracoes.PastaApi))
            {
                try
                {
                    form.MensagemRetorno = "\r\nCopiando a pasta da API\r\n";
                    form.AtualizaSaida();
                    await CopiaPastaApi();
                    form.MensagemRetorno = "API - OK\r\n";
                    form.AtualizaSaida();
                }
                catch (Exception pastaApi)
                {
                    form.MensagemRetorno = "\r\nFalha ao copiar a pasta da API: " + pastaApi.Message + "\n";
                    form.AtualizaSaida();
                }
            }
            if (configuracoes.PastaSite != null && Directory.Exists(configuracoes.PastaSite))
            {
                try
                {
                    form.MensagemRetorno = "\r\nCopiando pasta do Site\r\n";
                    form.AtualizaSaida();
                    await CopiaPastaSite();
                    form.MensagemRetorno = "SITE - OK\r\n";
                    form.AtualizaSaida();
                }
                catch (Exception pastaSite)
                {
                    form.MensagemRetorno = "Falha ao copiar pasta do SITE: " + pastaSite.Message + "\r\n";
                    form.AtualizaSaida();
                }
            }
            if (configuracoes.IpHost != null)
            {
                try
                {
                    form.MensagemRetorno = "\r\nFazendo backup da BASE\r\n";
                    form.AtualizaSaida();
                    await FazBackupBanco();
                    form.MensagemRetorno = "BASE - OK\r\n";
                    form.AtualizaSaida();
                }
                catch (Exception pastaBanco)
                {
                    form.MensagemRetorno = "Falha ao fazer backup da base: " + pastaBanco.Message + "\r\n";
                    form.AtualizaSaida();
                }
            }
            try {
                form.MensagemRetorno = "\r\nCompactando backup\r\n";
                form.AtualizaSaida();
                await ZipaArquivo();
                form.MensagemRetorno = "Backup compactado com sucesso\r\n";
                form.AtualizaSaida();
            }
            catch(Exception erroZip) {
                form.MensagemRetorno = "Falha ao compactar backup\r\n";
                form.AtualizaSaida();
            }
            try
            {
                LimpaTemp();
                form.MensagemRetorno = "\r\nPasta temporária excuída com sucesso!\r\n";
                form.AtualizaSaida();
                Process.Start(configuracoes.PastaDestino);
                
            }
            catch(Exception erroLimpeza)
            {
                form.MensagemRetorno = "Falha ao excluir arquivos temporários";
                form.AtualizaSaida();
            }
            form.FinalizaBackup();
        }

        public void IniciaBackupSilencioso()
        {
            if (Directory.Exists(configuracoes.PastaTemporaria))
            {
                LimpaTemp();
            }
            CopiaPastaApiSilencioso();
            CopiaPastaSiteSilencioso();
            FazBackupBancoSilencioso();
            ZipaArquivoSilencioso();
            LimpaTemp();
        }

        private async Task CopiaPastaApi() {
           
            if (!Directory.Exists(configuracoes.PastaTemporaria + "\\API\\")) {
                Directory.CreateDirectory(configuracoes.PastaTemporaria + "\\API\\");
            }
            await Task.Run(() => {
                foreach (string novoCaminho in Directory.GetDirectories(configuracoes.PastaApi, "*", SearchOption.AllDirectories)) {
                    Directory.CreateDirectory(novoCaminho.Replace(configuracoes.PastaApi, configuracoes.PastaTemporaria + "\\API\\"));
                }
                foreach (string novoCaminho in Directory.GetFiles(configuracoes.PastaApi, "*", SearchOption.AllDirectories)) {
                    File.Copy(novoCaminho, novoCaminho.Replace(configuracoes.PastaApi, configuracoes.PastaTemporaria + "\\API"), true);
                }
            });
            
        }
        private async Task CopiaPastaSite() {
            
            if (!Directory.Exists(configuracoes.PastaTemporaria + "\\SITE\\")) {
                Directory.CreateDirectory(configuracoes.PastaTemporaria + "\\SITE\\");
            }
            await Task.Run(() => {
                foreach (string novoCaminho in Directory.GetDirectories(configuracoes.PastaSite, "*", SearchOption.AllDirectories)) {
                    Directory.CreateDirectory(novoCaminho.Replace(configuracoes.PastaSite, configuracoes.PastaTemporaria + "\\SITE\\"));
                }
                foreach (string novoCaminho in Directory.GetFiles(configuracoes.PastaSite, "*", SearchOption.AllDirectories)) {
                    File.Copy(novoCaminho, novoCaminho.Replace(configuracoes.PastaSite, configuracoes.PastaTemporaria + "\\SITE\\"), true);
                }
            });
        }

        private async Task FazBackupBanco() {
            await Task.Run(() => {
                if (!Directory.Exists(configuracoes.PastaTemporaria + "\\BASE\\")) {
                    Directory.CreateDirectory(configuracoes.PastaTemporaria + "\\BASE\\");
                }
                if (!Directory.Exists(configuracoes.PastaTemporaria + "\\BASE\\")) {
                    Directory.CreateDirectory(configuracoes.PastaTemporaria + "\\BASE\\");
                }

                //CHAMA OS BATCHS EM PROCESSOS
                //Formato P
                parametrosBackup = "--host=" + configuracoes.IpHost + " --port=" + configuracoes.Porta + " --format=p --file=" + configuracoes.PastaTemporaria + "\\BASE\\baseP.backup --dbname=" + configuracoes.NomeBase + " --username=" + configuracoes.UserBanco;
                Process proc = new Process();
                Environment.SetEnvironmentVariable("PGPASSWORD", configuracoes.SenhaBanco);
                proc.StartInfo.FileName = configuracoes.CaminhoPostgre + "\\pg_dump.exe";
                proc.StartInfo.Arguments = parametrosBackup;
                proc.StartInfo.CreateNoWindow = true;
                proc.Start();
                proc.WaitForExit();
                proc.Close();

                //Formato C
                parametrosBackup = "--host=" + configuracoes.IpHost + " --port=" + configuracoes.Porta + " --format=c --file=" + configuracoes.PastaTemporaria + "\\BASE\\baseC.backup --dbname=" + configuracoes.NomeBase + "--username=" + configuracoes.UserBanco;
                Environment.SetEnvironmentVariable("PGPASSWORD", configuracoes.SenhaBanco);
                proc.StartInfo.FileName = configuracoes.CaminhoPostgre + "\\pg_dump.exe";
                proc.StartInfo.Arguments = parametrosBackup;
                proc.StartInfo.CreateNoWindow = true;
                proc.Start();
                proc.WaitForExit();
                proc.Close();

                //Formato T
                parametrosBackup = "--host=" + configuracoes.IpHost + " --port=" + configuracoes.Porta + " --format=t --file=" + configuracoes.PastaTemporaria + "\\BASE\\baseT.backup --dbname=" + configuracoes.NomeBase + " --username=" + configuracoes.UserBanco;
                Environment.SetEnvironmentVariable("PGPASSWORD", configuracoes.SenhaBanco);
                proc.StartInfo.FileName = configuracoes.CaminhoPostgre + "\\pg_dump.exe";
                proc.StartInfo.Arguments = parametrosBackup;
                proc.StartInfo.CreateNoWindow = true;
                proc.Start();
                proc.WaitForExit();
                proc.Close();
                /*foreach (string arquivo in Directory.GetFiles(configuracoes.PastaTemporaria + "\\BASE\\")) {
                    string Nomearquivo = Path.GetFileName(arquivo);
                    string destinoCaminho = Path.Combine(configuracoes.PastaDestino + "\\BASE\\", Nomearquivo);
                    File.Copy(arquivo, destinoCaminho, true);
                }*/
            });
        }

        private async Task ZipaArquivo() {
            if (!Directory.Exists(configuracoes.PastaDestino)) {
                Directory.CreateDirectory(configuracoes.PastaDestino);
            }
            string dataBackup = System.DateTime.Now.ToString("dd_MM_yy--hh_mm_ss");
            await Task.Run(() => {
                ZipFile.CreateFromDirectory(configuracoes.PastaTemporaria, configuracoes.PastaDestino + "\\backup_"+ configuracoes.NomeBase + dataBackup + ".zip", CompressionLevel.NoCompression,false);
            });
            
        }

        private void CopiaPastaApiSilencioso()
        {            
            if (!Directory.Exists(configuracoes.PastaTemporaria + "\\API\\"))
            {
                Directory.CreateDirectory(configuracoes.PastaTemporaria + "\\API\\");
            }
            if (configuracoes.PastaApi != null && Directory.Exists(configuracoes.PastaApi))
            {
                foreach (string novoCaminho in Directory.GetDirectories(configuracoes.PastaApi, "*", SearchOption.AllDirectories))
                {
                    Directory.CreateDirectory(novoCaminho.Replace(configuracoes.PastaApi, configuracoes.PastaTemporaria + "\\API\\"));
                }
                foreach (string novoCaminho in Directory.GetFiles(configuracoes.PastaApi, "*", SearchOption.AllDirectories))
                {
                    File.Copy(novoCaminho, novoCaminho.Replace(configuracoes.PastaApi, configuracoes.PastaTemporaria + "\\API\\"), true);
                }
            }

        }
        private void CopiaPastaSiteSilencioso()
        {
            if (!Directory.Exists(configuracoes.PastaTemporaria + "\\SITE\\"))
            {
                Directory.CreateDirectory(configuracoes.PastaTemporaria + "\\SITE\\");
            }
            if (configuracoes.PastaSite != null && Directory.Exists(configuracoes.PastaSite))
            {
                foreach (string novoCaminho in Directory.GetDirectories(configuracoes.PastaSite, "*", SearchOption.AllDirectories))
                {
                    Directory.CreateDirectory(novoCaminho.Replace(configuracoes.PastaSite, configuracoes.PastaTemporaria + "\\SITE\\"));
                }
                foreach (string novoCaminho in Directory.GetFiles(configuracoes.PastaSite, "*", SearchOption.AllDirectories))
                {
                    File.Copy(novoCaminho, novoCaminho.Replace(configuracoes.PastaSite, configuracoes.PastaTemporaria + "\\SITE\\"), true);
                }
            }
        }

        private void FazBackupBancoSilencioso()
        {
            if (!Directory.Exists(configuracoes.PastaTemporaria + "\\BASE\\"))
            {
                Directory.CreateDirectory(configuracoes.PastaTemporaria + "\\BASE\\");
            }
            if (configuracoes.IpHost != null)
            {
                //CHAMA OS BATCHS EM PROCESSOS
                //Formato P
                parametrosBackup = "--host=" + configuracoes.IpHost + " --port=" + configuracoes.Porta + " --format=p --file=" + configuracoes.PastaTemporaria + "\\BASE\\baseP.backup --dbname=" + configuracoes.NomeBase + " --username=" + configuracoes.UserBanco;
                Process proc = new Process();
                Environment.SetEnvironmentVariable("PGPASSWORD", configuracoes.SenhaBanco);
                proc.StartInfo.FileName = configuracoes.CaminhoPostgre + "\\pg_dump.exe";
                proc.StartInfo.Arguments = parametrosBackup;
                proc.StartInfo.CreateNoWindow = true;
                proc.Start();
                proc.WaitForExit();
                proc.Close();

                //Formato C
                parametrosBackup = "--host=" + configuracoes.IpHost + " --port=" + configuracoes.Porta + " --format=c --file=" + configuracoes.PastaTemporaria + "\\BASE\\baseC.backup --dbname=" + configuracoes.NomeBase + "--username=" + configuracoes.UserBanco;
                Environment.SetEnvironmentVariable("PGPASSWORD", configuracoes.SenhaBanco);
                proc.StartInfo.FileName = configuracoes.CaminhoPostgre + "\\pg_dump.exe";
                proc.StartInfo.Arguments = parametrosBackup;
                proc.StartInfo.CreateNoWindow = true;
                proc.Start();
                proc.WaitForExit();
                proc.Close();

                //Formato T
                parametrosBackup = "--host=" + configuracoes.IpHost + " --port=" + configuracoes.Porta + " --format=t --file=" + configuracoes.PastaTemporaria + "\\BASE\\baseT.backup --dbname=" + configuracoes.NomeBase + " --username=" + configuracoes.UserBanco;
                Environment.SetEnvironmentVariable("PGPASSWORD", configuracoes.SenhaBanco);
                proc.StartInfo.FileName = configuracoes.CaminhoPostgre + "\\pg_dump.exe";
                proc.StartInfo.Arguments = parametrosBackup;
                proc.StartInfo.CreateNoWindow = true;
                proc.Start();
                proc.WaitForExit();
                proc.Close();
                /*foreach (string arquivo in Directory.GetFiles(configuracoes.PastaTemporaria + "\\BASE\\"))
                {
                    string Nomearquivo = Path.GetFileName(arquivo);
                    string destinoCaminho = Path.Combine(configuracoes.PastaTemporaria + "\\BASE\\", Nomearquivo);
                    File.Copy(arquivo, destinoCaminho, true);
                }*/
            } 
        }

        private void ZipaArquivoSilencioso()
        {
            if (!Directory.Exists(configuracoes.PastaDestino))
            {
                Directory.CreateDirectory(configuracoes.PastaDestino);
            }
            string dataBackup = System.DateTime.Now.ToString("dd_MM_yy--hh_mm_ss");
            ZipFile.CreateFromDirectory(configuracoes.PastaTemporaria, configuracoes.PastaDestino + "\\backup_" + configuracoes.NomeBase + dataBackup + ".zip", CompressionLevel.NoCompression, false);
        }

        private void LimpaTemp()
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(configuracoes.PastaTemporaria + "\\BASE\\");
            if (di.Exists)
            {
                foreach (DirectoryInfo dir in di.GetDirectories()) 
                {
                    dir.Attributes &= ~FileAttributes.ReadOnly;
                    foreach (FileInfo file in dir.GetFiles())
                    {
                        File.SetAttributes(file.FullName, FileAttributes.Normal);
                        file.Delete();
                    }
                    di.Refresh();
                }
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Attributes &= ~FileAttributes.ReadOnly;                        
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }
                Directory.Delete(configuracoes.PastaTemporaria + "\\BASE\\");
            }

            di = new DirectoryInfo(configuracoes.PastaTemporaria + "\\API\\");
            if (di.Exists)
            {
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Attributes &= ~FileAttributes.ReadOnly;
                    foreach (FileInfo file in dir.GetFiles())
                    {
                        file.Attributes &= ~FileAttributes.ReadOnly;

                    }
                    di.Refresh();
                }
                foreach (FileInfo file in di.GetFiles())
                {
                    File.SetAttributes(file.FullName, FileAttributes.Normal);
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }
                Directory.Delete(configuracoes.PastaTemporaria + "\\API\\");
            }

            di = new DirectoryInfo(configuracoes.PastaTemporaria + "\\SITE\\");
            if (di.Exists)
            {
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Attributes &= ~FileAttributes.ReadOnly;
                    foreach (FileInfo file in dir.GetFiles())
                    {
                        file.Attributes &= ~FileAttributes.ReadOnly;

                    }
                    di.Refresh();
                }
                foreach (FileInfo file in di.GetFiles())
                {
                    File.SetAttributes(file.FullName, FileAttributes.Normal);
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);

                }
                Directory.Delete(configuracoes.PastaTemporaria + "\\SITE\\");

                di = new DirectoryInfo(configuracoes.PastaTemporaria);
                di.Delete();
                
            }
        }

    }
}
