﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Api_backuper {
    public class FazBackup {
        ConfiguracaoModel configuracoes;
        string caminhoBackupBase = "C:\\baseBackup\\";
        string parametrosBackup;
        Form1 form;
        //public Boolean status { get; set; }
        public FazBackup(ConfiguracaoModel _configuracoes, Form1 _form){
            configuracoes = _configuracoes;
            form = _form;            
        }
        public FazBackup(ConfiguracaoModel _configuracoes)
        {
            configuracoes = _configuracoes;
            //status = false;
            
        }

        public async Task IniciaBackup() {
            try {
                form.MensagemRetorno = "\nCopiando a pasta da API\n";
                form.AtualizaSaida();
                await CopiaPastaApi();
                form.MensagemRetorno = "API - OK\n";
                form.AtualizaSaida();
            }
            catch(Exception pastaApi) {
                form.MensagemRetorno = "\nFalha ao copiar a pasta da API: " + pastaApi.Message+"\n";
                form.AtualizaSaida();
            }
            try {
                form.MensagemRetorno = "Copiando pasta do Site\n";
                form.AtualizaSaida();
                await CopiaPastaSite();
                form.MensagemRetorno = "SITE - OK\n";
                form.AtualizaSaida();
            }
            catch (Exception pastaSite) {
                form.MensagemRetorno = "Falha ao copiar pasta do SITE: " + pastaSite.Message+"\n";
                form.AtualizaSaida();
            }
            try {
                form.MensagemRetorno = "Fazendo backup da BASE\n";
                form.AtualizaSaida();
                await FazBackupBanco();
                form.MensagemRetorno = "Fazendo backup da BASE - OK\n";
                form.AtualizaSaida();
            }
            catch (Exception pastaBanco) {
                form.MensagemRetorno = "Falha ao fazer backup da base: "+pastaBanco.Message+"\n";
                form.AtualizaSaida();
            }
            try {
                form.MensagemRetorno = "Compactando backup\n";
                form.AtualizaSaida();
                await ZipaArquivo();
                form.MensagemRetorno = "Backup compactado com sucesso\n";
                form.AtualizaSaida();
            }
            catch(Exception erroZip) {
                form.MensagemRetorno = "Falha ao compactar backup\n";
                form.AtualizaSaida();
            }
            Process.Start(configuracoes.PastaDestino);
            form.FinalizaBackup();
        }

        public void IniciaBackupSilencioso()
        {
            CopiaPastaApiSilencioso();
            CopiaPastaSiteSilencioso();
            FazBackupBancoSilencioso();
            ZipaArquivoSilencioso();                
            
             

        }

        private async Task CopiaPastaApi() {
            if (Directory.Exists(configuracoes.PastaTemporaria + "\\API\\"))
            {
                System.IO.DirectoryInfo di = new DirectoryInfo(configuracoes.PastaTemporaria + "\\API\\");
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }
                Directory.Delete(configuracoes.PastaTemporaria + "\\API\\");
            }
            if (!Directory.Exists(configuracoes.PastaTemporaria + "\\API\\")) {
                Directory.CreateDirectory(configuracoes.PastaTemporaria + "\\API\\");
            }
            await Task.Run(() => {
                foreach (string novoCaminho in Directory.GetDirectories(configuracoes.PastaApi, "*", SearchOption.AllDirectories)) {
                    Directory.CreateDirectory(novoCaminho.Replace(configuracoes.PastaApi, configuracoes.PastaTemporaria + "\\API\\"));
                }
                foreach (string novoCaminho in Directory.GetFiles(configuracoes.PastaApi, "*", SearchOption.AllDirectories)) {
                    File.Copy(novoCaminho, novoCaminho.Replace(configuracoes.PastaApi, configuracoes.PastaTemporaria + "\\API\\"), true);
                }
            });
            
        }
        private async Task CopiaPastaSite() {
            if (Directory.Exists(configuracoes.PastaTemporaria + "\\SITE\\"))
            {
                System.IO.DirectoryInfo di = new DirectoryInfo(configuracoes.PastaTemporaria + "\\SITE\\");
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }
                Directory.Delete(configuracoes.PastaTemporaria + "\\SITE\\");
            }
            if (!Directory.Exists(configuracoes.PastaTemporaria + "\\SITE\\")) {
                Directory.CreateDirectory(configuracoes.PastaTemporaria + "\\SITE\\");
            }
            await Task.Run(() => {
                foreach (string novoCaminho in Directory.GetDirectories(configuracoes.PastaSite, "*", SearchOption.AllDirectories)) {
                    Directory.CreateDirectory(novoCaminho.Replace(configuracoes.PastaSite, configuracoes.PastaTemporaria + "\\SITE\\"));
                }
                foreach (string novoCaminho in Directory.GetFiles(configuracoes.PastaSite, "*", SearchOption.AllDirectories)) {
                    File.Copy(novoCaminho, novoCaminho.Replace(configuracoes.PastaSite, configuracoes.PastaTemporaria + "\\SITE\\"), true);
                }
            });
        }

        private async Task FazBackupBanco() {
            await Task.Run(() => {
                if (!Directory.Exists(caminhoBackupBase)) {
                    Directory.CreateDirectory(caminhoBackupBase);
                }

                if (Directory.Exists(configuracoes.PastaTemporaria + "\\BASE\\")) {
                    System.IO.DirectoryInfo di = new DirectoryInfo(configuracoes.PastaTemporaria + "\\BASE\\");
                    foreach (FileInfo file in di.GetFiles())
                    {
                        file.Delete();
                    }
                    foreach (DirectoryInfo dir in di.GetDirectories())
                    {
                        dir.Delete(true);
                    }
                    Directory.Delete(configuracoes.PastaTemporaria + "\\BASE\\");
                }
                if (!Directory.Exists(configuracoes.PastaTemporaria + "\\BASE\\")) {
                    Directory.CreateDirectory(configuracoes.PastaTemporaria + "\\BASE\\");
                }

                //CHAMA OS BATCHS EM PROCESSOS
                //Formato P
                parametrosBackup = "--host" + configuracoes.IpHost + "--port" + configuracoes.Porta + "-username" + configuracoes.UserBanco + "--format p --file" + caminhoBackupBase + "\baseP.backup " + configuracoes.NomeBase;
                Process proc = new Process();
                Environment.SetEnvironmentVariable("PGPASSWORD", configuracoes.SenhaBanco);
                proc.StartInfo.FileName = configuracoes.CaminhoPostgre + "\\pg_dump.exe";
                proc.StartInfo.Arguments = parametrosBackup;
                proc.StartInfo.CreateNoWindow = true;
                proc.Start();
                proc.WaitForExit();
                proc.Close();

                //Formato C
                parametrosBackup = "--host" + configuracoes.IpHost + "--port" + configuracoes.Porta + "-username" + configuracoes.UserBanco + "--format c --file" + caminhoBackupBase + "\baseC.backup " + configuracoes.NomeBase;
                Environment.SetEnvironmentVariable("PGPASSWORD", configuracoes.SenhaBanco);
                proc.StartInfo.FileName = configuracoes.CaminhoPostgre + "\\pg_dump.exe";
                proc.StartInfo.Arguments = parametrosBackup;
                proc.StartInfo.CreateNoWindow = true;
                proc.Start();
                proc.WaitForExit();
                proc.Close();

                //Formato T
                parametrosBackup = "--host" + configuracoes.IpHost + "--port" + configuracoes.Porta + "-username" + configuracoes.UserBanco + "--format t --file" + caminhoBackupBase + "\baseT.backup " + configuracoes.NomeBase;
                Environment.SetEnvironmentVariable("PGPASSWORD", configuracoes.SenhaBanco);
                proc.StartInfo.FileName = configuracoes.CaminhoPostgre + "\\pg_dump.exe";
                proc.StartInfo.Arguments = parametrosBackup;
                proc.StartInfo.CreateNoWindow = true;
                proc.Start();
                proc.WaitForExit();
                proc.Close();
                foreach (string arquivo in Directory.GetFiles(caminhoBackupBase)) {
                    string Nomearquivo = Path.GetFileName(arquivo);
                    string destinoCaminho = Path.Combine(configuracoes.PastaTemporaria + "\\BASE\\", Nomearquivo);
                    File.Copy(arquivo, destinoCaminho, true);
                }
            });
        }

        private async Task ZipaArquivo() {
            if (!Directory.Exists(configuracoes.PastaDestino)) {
                Directory.CreateDirectory(configuracoes.PastaDestino);
            }
            string dataBackup = System.DateTime.Now.ToString("dd_MM_yy--hh_mm_ss");
            await Task.Run(() => {
                ZipFile.CreateFromDirectory(configuracoes.PastaTemporaria, configuracoes.PastaDestino + "\\backup"+ dataBackup + ".zip", CompressionLevel.NoCompression,false);
            });
            
        }

        private void CopiaPastaApiSilencioso()
        {
            if (Directory.Exists(configuracoes.PastaTemporaria + "\\API\\"))
            {
                System.IO.DirectoryInfo di = new DirectoryInfo(configuracoes.PastaTemporaria + "\\API\\");
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }
                Directory.Delete(configuracoes.PastaTemporaria + "\\API\\");
            }
            if (!Directory.Exists(configuracoes.PastaTemporaria + "\\API\\"))
            {
                Directory.CreateDirectory(configuracoes.PastaTemporaria + "\\API\\");
            }
            foreach (string novoCaminho in Directory.GetDirectories(configuracoes.PastaApi, "*", SearchOption.AllDirectories))
                {
                    Directory.CreateDirectory(novoCaminho.Replace(configuracoes.PastaApi, configuracoes.PastaTemporaria + "\\API\\"));
                }
            foreach (string novoCaminho in Directory.GetFiles(configuracoes.PastaApi, "*", SearchOption.AllDirectories))
            {
                File.Copy(novoCaminho, novoCaminho.Replace(configuracoes.PastaApi, configuracoes.PastaTemporaria + "\\API\\"), true);
            }
            

        }
        private void CopiaPastaSiteSilencioso()
        {
            if (Directory.Exists(configuracoes.PastaTemporaria + "\\SITE\\"))
            {
                System.IO.DirectoryInfo di = new DirectoryInfo(configuracoes.PastaTemporaria + "\\SITE\\");
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }
                Directory.Delete(configuracoes.PastaTemporaria + "\\SITE\\");
            }
            if (!Directory.Exists(configuracoes.PastaTemporaria + "\\SITE\\"))
            {
                Directory.CreateDirectory(configuracoes.PastaTemporaria + "\\SITE\\");
            }
            
            foreach (string novoCaminho in Directory.GetDirectories(configuracoes.PastaSite, "*", SearchOption.AllDirectories))
            {
                Directory.CreateDirectory(novoCaminho.Replace(configuracoes.PastaSite, configuracoes.PastaTemporaria + "\\SITE\\"));
            }
            foreach (string novoCaminho in Directory.GetFiles(configuracoes.PastaSite, "*", SearchOption.AllDirectories))
            {
                File.Copy(novoCaminho, novoCaminho.Replace(configuracoes.PastaSite, configuracoes.PastaTemporaria + "\\SITE\\"), true);
            }
           
        }

        private void FazBackupBancoSilencioso()
        {
            
            if (!Directory.Exists(caminhoBackupBase))
            {
                Directory.CreateDirectory(caminhoBackupBase);
            }

            if (Directory.Exists(configuracoes.PastaTemporaria + "\\BASE\\"))
            {
                System.IO.DirectoryInfo di = new DirectoryInfo(configuracoes.PastaTemporaria + "\\BASE\\");
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }
                Directory.Delete(configuracoes.PastaTemporaria + "\\BASE\\");
            }
            if (!Directory.Exists(configuracoes.PastaTemporaria + "\\BASE\\"))
            {
                Directory.CreateDirectory(configuracoes.PastaTemporaria + "\\BASE\\");
            }

            //CHAMA OS BATCHS EM PROCESSOS
            //Formato P
            parametrosBackup = "--host" + configuracoes.IpHost + "--port" + configuracoes.Porta + "-username" + configuracoes.UserBanco + "--format p --file" + caminhoBackupBase + "\baseP.backup " + configuracoes.NomeBase;
            Process proc = new Process();
            Environment.SetEnvironmentVariable("PGPASSWORD", configuracoes.SenhaBanco);
            proc.StartInfo.FileName = configuracoes.CaminhoPostgre + "\\pg_dump.exe";
            proc.StartInfo.Arguments = parametrosBackup;
            proc.StartInfo.CreateNoWindow = true;
            proc.Start();
            proc.WaitForExit();
            proc.Close();

            //Formato C
            parametrosBackup = "--host" + configuracoes.IpHost + "--port" + configuracoes.Porta + "-username" + configuracoes.UserBanco + "--format c --file" + caminhoBackupBase + "\baseC.backup " + configuracoes.NomeBase;
            Environment.SetEnvironmentVariable("PGPASSWORD", configuracoes.SenhaBanco);
            proc.StartInfo.FileName = configuracoes.CaminhoPostgre + "\\pg_dump.exe";
            proc.StartInfo.Arguments = parametrosBackup;
            proc.StartInfo.CreateNoWindow = true;
            proc.Start();
            proc.WaitForExit();
            proc.Close();

            //Formato T
            parametrosBackup = "--host" + configuracoes.IpHost + "--port" + configuracoes.Porta + "-username" + configuracoes.UserBanco + "--format t --file" + caminhoBackupBase + "\baseT.backup " + configuracoes.NomeBase;
            Environment.SetEnvironmentVariable("PGPASSWORD", configuracoes.SenhaBanco);
            proc.StartInfo.FileName = configuracoes.CaminhoPostgre + "\\pg_dump.exe";
            proc.StartInfo.Arguments = parametrosBackup;
            proc.StartInfo.CreateNoWindow = true;
            proc.Start();
            proc.WaitForExit();
            proc.Close();
            foreach (string arquivo in Directory.GetFiles(caminhoBackupBase))
            {
                string Nomearquivo = Path.GetFileName(arquivo);
                string destinoCaminho = Path.Combine(configuracoes.PastaTemporaria + "\\BASE\\", Nomearquivo);
                File.Copy(arquivo, destinoCaminho, true);
            }
           
        }

        private void ZipaArquivo()
        {
            if (!Directory.Exists(configuracoes.PastaDestino))
            {
                Directory.CreateDirectory(configuracoes.PastaDestino);
            }
            string dataBackup = System.DateTime.Now.ToString("dd_MM_yy--hh_mm_ss");
            ZipFile.CreateFromDirectory(configuracoes.PastaTemporaria, configuracoes.PastaDestino + "\\backup" + dataBackup + ".zip", CompressionLevel.NoCompression, false);
        }



    }
}
