﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api_backuper {
    public class ConfiguracaoModel {
        public string PastaSite { get; set; }
        public string PastaApi { get; set; }
        public string PastaDestino { get; set; }
        public string PastaTemporaria { get {
                return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\AlfaMeta\\temp";
            }
        }
        public string NomeBase { get;  set; }
        public string CaminhoPostgre { get; set; }
        public string SenhaBanco { get; set; }
        public string UserBanco { get; set; }
        public string IpHost { get; set; }
        public string Porta { get; set; }
        public string[] Format { get; set; }
    }
}
