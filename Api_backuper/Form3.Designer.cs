﻿namespace Api_backuper
{
    partial class FormAgendaBackup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxUserAdm = new System.Windows.Forms.TextBox();
            this.labelUser = new System.Windows.Forms.Label();
            this.labelSenha = new System.Windows.Forms.Label();
            this.textBoxSenhaAdm = new System.Windows.Forms.TextBox();
            this.labelHora = new System.Windows.Forms.Label();
            this.textBoxHora = new System.Windows.Forms.TextBox();
            this.buttonSalvar = new System.Windows.Forms.Button();
            this.buttonCancelar = new System.Windows.Forms.Button();
            this.checkBoxAtivar = new System.Windows.Forms.CheckBox();
            this.lblAvisoAgenda = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxUserAdm
            // 
            this.textBoxUserAdm.Location = new System.Drawing.Point(179, 14);
            this.textBoxUserAdm.Name = "textBoxUserAdm";
            this.textBoxUserAdm.Size = new System.Drawing.Size(204, 20);
            this.textBoxUserAdm.TabIndex = 0;
            // 
            // labelUser
            // 
            this.labelUser.AutoSize = true;
            this.labelUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.labelUser.Location = new System.Drawing.Point(7, 14);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(146, 15);
            this.labelUser.TabIndex = 1;
            this.labelUser.Text = "Usuário do Administrador";
            this.labelUser.Click += new System.EventHandler(this.Label1_Click);
            // 
            // labelSenha
            // 
            this.labelSenha.AutoSize = true;
            this.labelSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.labelSenha.Location = new System.Drawing.Point(7, 53);
            this.labelSenha.Name = "labelSenha";
            this.labelSenha.Size = new System.Drawing.Size(139, 15);
            this.labelSenha.TabIndex = 3;
            this.labelSenha.Text = "Senha do Administrador";
            // 
            // textBoxSenhaAdm
            // 
            this.textBoxSenhaAdm.Location = new System.Drawing.Point(179, 53);
            this.textBoxSenhaAdm.Name = "textBoxSenhaAdm";
            this.textBoxSenhaAdm.PasswordChar = '*';
            this.textBoxSenhaAdm.Size = new System.Drawing.Size(204, 20);
            this.textBoxSenhaAdm.TabIndex = 2;
            // 
            // labelHora
            // 
            this.labelHora.AutoSize = true;
            this.labelHora.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.labelHora.Location = new System.Drawing.Point(7, 95);
            this.labelHora.Name = "labelHora";
            this.labelHora.Size = new System.Drawing.Size(168, 15);
            this.labelHora.TabIndex = 5;
            this.labelHora.Text = "Hora do Backup (HH:MM:SS)";
            // 
            // textBoxHora
            // 
            this.textBoxHora.Location = new System.Drawing.Point(179, 94);
            this.textBoxHora.Name = "textBoxHora";
            this.textBoxHora.Size = new System.Drawing.Size(204, 20);
            this.textBoxHora.TabIndex = 4;
            this.textBoxHora.TextChanged += new System.EventHandler(this.TextBoxHora_TextChanged);
            // 
            // buttonSalvar
            // 
            this.buttonSalvar.Location = new System.Drawing.Point(79, 201);
            this.buttonSalvar.Name = "buttonSalvar";
            this.buttonSalvar.Size = new System.Drawing.Size(90, 26);
            this.buttonSalvar.TabIndex = 6;
            this.buttonSalvar.Text = "Salvar";
            this.buttonSalvar.UseVisualStyleBackColor = true;
            this.buttonSalvar.Click += new System.EventHandler(this.ButtonSalvar_Click);
            // 
            // buttonCancelar
            // 
            this.buttonCancelar.Location = new System.Drawing.Point(212, 201);
            this.buttonCancelar.Name = "buttonCancelar";
            this.buttonCancelar.Size = new System.Drawing.Size(90, 26);
            this.buttonCancelar.TabIndex = 7;
            this.buttonCancelar.Text = "Cancelar";
            this.buttonCancelar.UseVisualStyleBackColor = true;
            this.buttonCancelar.Click += new System.EventHandler(this.ButtonCancelar_Click);
            // 
            // checkBoxAtivar
            // 
            this.checkBoxAtivar.AutoSize = true;
            this.checkBoxAtivar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.checkBoxAtivar.Location = new System.Drawing.Point(12, 138);
            this.checkBoxAtivar.Name = "checkBoxAtivar";
            this.checkBoxAtivar.Size = new System.Drawing.Size(209, 19);
            this.checkBoxAtivar.TabIndex = 8;
            this.checkBoxAtivar.Text = "Ativar o Agendamento Automático";
            this.checkBoxAtivar.UseVisualStyleBackColor = true;
            this.checkBoxAtivar.CheckedChanged += new System.EventHandler(this.CheckBox1_CheckedChanged);
            // 
            // lblAvisoAgenda
            // 
            this.lblAvisoAgenda.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.lblAvisoAgenda.Location = new System.Drawing.Point(7, 160);
            this.lblAvisoAgenda.Name = "lblAvisoAgenda";
            this.lblAvisoAgenda.Size = new System.Drawing.Size(376, 38);
            this.lblAvisoAgenda.TabIndex = 9;
            this.lblAvisoAgenda.Text = "Deixar essa opção desativada irá excluir os agendamentos já existentes, caso haja" +
    "m.";
            // 
            // FormAgendaBackup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(397, 240);
            this.Controls.Add(this.lblAvisoAgenda);
            this.Controls.Add(this.checkBoxAtivar);
            this.Controls.Add(this.buttonCancelar);
            this.Controls.Add(this.buttonSalvar);
            this.Controls.Add(this.labelHora);
            this.Controls.Add(this.textBoxHora);
            this.Controls.Add(this.labelSenha);
            this.Controls.Add(this.textBoxSenhaAdm);
            this.Controls.Add(this.labelUser);
            this.Controls.Add(this.textBoxUserAdm);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormAgendaBackup";
            this.Text = "Agendar Backup";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxUserAdm;
        private System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.Label labelSenha;
        private System.Windows.Forms.TextBox textBoxSenhaAdm;
        private System.Windows.Forms.Label labelHora;
        private System.Windows.Forms.TextBox textBoxHora;
        private System.Windows.Forms.Button buttonSalvar;
        private System.Windows.Forms.Button buttonCancelar;
        private System.Windows.Forms.CheckBox checkBoxAtivar;
        private System.Windows.Forms.Label lblAvisoAgenda;
    }
}