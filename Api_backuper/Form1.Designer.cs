﻿namespace Api_backuper {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnSalvar = new System.Windows.Forms.Button();
            this.btnFazBackup = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPastaApi = new System.Windows.Forms.TextBox();
            this.txtPastaSite = new System.Windows.Forms.TextBox();
            this.txtDestinoCopia = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSaida = new System.Windows.Forms.TextBox();
            this.buttonConfigurarBase = new System.Windows.Forms.Button();
            this.btnDestinoCopia = new System.Windows.Forms.Button();
            this.btnPastaSite = new System.Windows.Forms.Button();
            this.btnPastaApi = new System.Windows.Forms.Button();
            this.buttonRestaurar = new System.Windows.Forms.Button();
            this.buttonAgendamento = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSalvar
            // 
            this.btnSalvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalvar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSalvar.Location = new System.Drawing.Point(130, 245);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(274, 35);
            this.btnSalvar.TabIndex = 3;
            this.btnSalvar.Text = "1-Salvar configurações";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnFazBackup
            // 
            this.btnFazBackup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFazBackup.Location = new System.Drawing.Point(130, 286);
            this.btnFazBackup.Name = "btnFazBackup";
            this.btnFazBackup.Size = new System.Drawing.Size(274, 35);
            this.btnFazBackup.TabIndex = 8;
            this.btnFazBackup.Text = "2 - Fazer Backup";
            this.btnFazBackup.UseVisualStyleBackColor = true;
            this.btnFazBackup.Click += new System.EventHandler(this.button2_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 16);
            this.label4.TabIndex = 9;
            this.label4.Text = "Pasta da API";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 16);
            this.label5.TabIndex = 10;
            this.label5.Text = "Pasta Site";
            // 
            // txtPastaApi
            // 
            this.txtPastaApi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPastaApi.Location = new System.Drawing.Point(111, 11);
            this.txtPastaApi.Name = "txtPastaApi";
            this.txtPastaApi.Size = new System.Drawing.Size(370, 22);
            this.txtPastaApi.TabIndex = 11;
            // 
            // txtPastaSite
            // 
            this.txtPastaSite.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPastaSite.Location = new System.Drawing.Point(111, 34);
            this.txtPastaSite.Name = "txtPastaSite";
            this.txtPastaSite.Size = new System.Drawing.Size(370, 22);
            this.txtPastaSite.TabIndex = 12;
            // 
            // txtDestinoCopia
            // 
            this.txtDestinoCopia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDestinoCopia.Location = new System.Drawing.Point(111, 58);
            this.txtDestinoCopia.Name = "txtDestinoCopia";
            this.txtDestinoCopia.Size = new System.Drawing.Size(370, 22);
            this.txtDestinoCopia.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 16);
            this.label6.TabIndex = 15;
            this.label6.Text = "Copiar para";
            // 
            // txtSaida
            // 
            this.txtSaida.BackColor = System.Drawing.Color.White;
            this.txtSaida.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSaida.Location = new System.Drawing.Point(15, 86);
            this.txtSaida.Multiline = true;
            this.txtSaida.Name = "txtSaida";
            this.txtSaida.ReadOnly = true;
            this.txtSaida.Size = new System.Drawing.Size(495, 153);
            this.txtSaida.TabIndex = 18;
            // 
            // buttonConfigurarBase
            // 
            this.buttonConfigurarBase.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.buttonConfigurarBase.Location = new System.Drawing.Point(15, 247);
            this.buttonConfigurarBase.Name = "buttonConfigurarBase";
            this.buttonConfigurarBase.Size = new System.Drawing.Size(94, 74);
            this.buttonConfigurarBase.TabIndex = 19;
            this.buttonConfigurarBase.Text = "Configurar Base de Dados";
            this.buttonConfigurarBase.UseVisualStyleBackColor = true;
            this.buttonConfigurarBase.Click += new System.EventHandler(this.Button1_Click_1);
            // 
            // btnDestinoCopia
            // 
            this.btnDestinoCopia.Image = global::Api_backuper.Properties.Resources.icons8_live_folder_16;
            this.btnDestinoCopia.Location = new System.Drawing.Point(483, 57);
            this.btnDestinoCopia.Name = "btnDestinoCopia";
            this.btnDestinoCopia.Size = new System.Drawing.Size(28, 23);
            this.btnDestinoCopia.TabIndex = 17;
            this.btnDestinoCopia.UseVisualStyleBackColor = true;
            this.btnDestinoCopia.Click += new System.EventHandler(this.btnDestinoCopia_Click);
            // 
            // btnPastaSite
            // 
            this.btnPastaSite.Image = global::Api_backuper.Properties.Resources.icons8_live_folder_16;
            this.btnPastaSite.Location = new System.Drawing.Point(483, 33);
            this.btnPastaSite.Name = "btnPastaSite";
            this.btnPastaSite.Size = new System.Drawing.Size(28, 23);
            this.btnPastaSite.TabIndex = 14;
            this.btnPastaSite.UseVisualStyleBackColor = true;
            this.btnPastaSite.Click += new System.EventHandler(this.btnPastaSite_Click);
            // 
            // btnPastaApi
            // 
            this.btnPastaApi.Image = global::Api_backuper.Properties.Resources.icons8_live_folder_16;
            this.btnPastaApi.Location = new System.Drawing.Point(483, 11);
            this.btnPastaApi.Name = "btnPastaApi";
            this.btnPastaApi.Size = new System.Drawing.Size(28, 23);
            this.btnPastaApi.TabIndex = 13;
            this.btnPastaApi.UseVisualStyleBackColor = true;
            this.btnPastaApi.Click += new System.EventHandler(this.btnPastaApi_Click);
            // 
            // buttonRestaurar
            // 
            this.buttonRestaurar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.buttonRestaurar.Location = new System.Drawing.Point(418, 245);
            this.buttonRestaurar.Name = "buttonRestaurar";
            this.buttonRestaurar.Size = new System.Drawing.Size(92, 75);
            this.buttonRestaurar.TabIndex = 20;
            this.buttonRestaurar.Text = "Restaurar Backup";
            this.buttonRestaurar.UseVisualStyleBackColor = true;
            this.buttonRestaurar.Click += new System.EventHandler(this.Button2_Click_1);
            // 
            // buttonAgendamento
            // 
            this.buttonAgendamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAgendamento.Location = new System.Drawing.Point(130, 327);
            this.buttonAgendamento.Name = "buttonAgendamento";
            this.buttonAgendamento.Size = new System.Drawing.Size(274, 35);
            this.buttonAgendamento.TabIndex = 21;
            this.buttonAgendamento.Text = "3 - Configurar Agendamento";
            this.buttonAgendamento.UseVisualStyleBackColor = true;
            this.buttonAgendamento.Click += new System.EventHandler(this.ButtonAgendamento_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(522, 368);
            this.Controls.Add(this.buttonAgendamento);
            this.Controls.Add(this.buttonRestaurar);
            this.Controls.Add(this.buttonConfigurarBase);
            this.Controls.Add(this.txtSaida);
            this.Controls.Add(this.btnDestinoCopia);
            this.Controls.Add(this.txtDestinoCopia);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnPastaSite);
            this.Controls.Add(this.btnPastaApi);
            this.Controls.Add(this.txtPastaSite);
            this.Controls.Add(this.txtPastaApi);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnFazBackup);
            this.Controls.Add(this.btnSalvar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alfameta Backuper";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Button btnFazBackup;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPastaApi;
        private System.Windows.Forms.TextBox txtPastaSite;
        private System.Windows.Forms.Button btnPastaApi;
        private System.Windows.Forms.Button btnPastaSite;
        private System.Windows.Forms.Button btnDestinoCopia;
        private System.Windows.Forms.TextBox txtDestinoCopia;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSaida;
        private System.Windows.Forms.Button buttonConfigurarBase;
        private System.Windows.Forms.Button buttonRestaurar;
        private System.Windows.Forms.Button buttonAgendamento;
    }
}

