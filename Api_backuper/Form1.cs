﻿using System;
using System.Windows.Forms;

namespace Api_backuper {
    public partial class Form1 : Form {
        Configurador configurador;
        ConfiguracaoModel configuracoes;
        FazBackup backup;
        RestauraBackup restaurador;
        public string MensagemRetorno { get; set; }
        public Form1() {
            InitializeComponent();
            configurador = new Configurador();
            configuracoes = configurador.LeArquivoConfiguracao();
            configuracoes = configurador.LeArquivoConfiguracaoServidor();
            if(configuracoes == null) {
                configuracoes = new ConfiguracaoModel();
            }
            else {
                txtPastaApi.Text = configuracoes.PastaApi;
                txtPastaSite.Text = configuracoes.PastaSite;
                txtDestinoCopia.Text = configuracoes.PastaDestino;
                backup = new FazBackup(configuracoes, this);
            }
            txtSaida.Text = "Esse processo demora aproximadamente 1 minuto dependendo do tamanho da API, Site e da conexão com o banco de dados!\r\n" +
                "Os arquivos serão copiados para uma pasta temporaria em C:\\Temp e então compactados para um arquivo ZIP no caminho selecionado como destino no configurador.\r\n";
            restaurador = new RestauraBackup(configuracoes,this);
        }
        
        public void AtualizaSaida() {
            txtSaida.AppendText(MensagemRetorno);
        }
        public void FinalizaBackup() {
            btnFazBackup.Enabled = true;
            btnSalvar.Enabled = true;
            buttonAgendamento.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e) {     
            
            if (string.IsNullOrEmpty(txtDestinoCopia.Text) || string.IsNullOrWhiteSpace(txtDestinoCopia.Text)) {
                MessageBox.Show("Por favor, informe o destino do backup");
            }
            else {                
                configurador.SalvaConfiguracoes(configuracoes);
                configuracoes.PastaApi = txtPastaApi.Text;
                configuracoes.PastaDestino = txtDestinoCopia.Text;
                configuracoes.PastaSite = txtPastaSite.Text;
                backup = new FazBackup(configuracoes, this);
                MensagemRetorno = "\nConfigurações Salvas com sucesso.\n";
                AtualizaSaida();
            }
            
        }

        private void btnPastaApi_Click(object sender, EventArgs e) {
            using(var pastaApi = new FolderBrowserDialog()) {
                DialogResult result = pastaApi.ShowDialog();
                if (result == DialogResult.OK) {
                    configuracoes.PastaApi = pastaApi.SelectedPath;
                    txtPastaApi.Text = pastaApi.SelectedPath;
                }
            }
        }

        private void btnPastaSite_Click(object sender, EventArgs e) {
            using (var pastaSite = new FolderBrowserDialog()) {
                DialogResult result = pastaSite.ShowDialog();
                if (result == DialogResult.OK) {
                    configuracoes.PastaSite = pastaSite.SelectedPath;
                    txtPastaSite.Text = pastaSite.SelectedPath;
                }
            }
        }

        private void btnDestinoCopia_Click(object sender, EventArgs e) {
            using (var PastaDestino = new FolderBrowserDialog()) {
                DialogResult result = PastaDestino.ShowDialog();
                if (result == DialogResult.OK) {
                    configuracoes.PastaDestino = PastaDestino.SelectedPath;
                    txtDestinoCopia.Text = PastaDestino.SelectedPath;
                }
            }
        }

        private async void button2_Click(object sender, EventArgs e) {
            btnFazBackup.Enabled = false;
            btnSalvar.Enabled = false;
            buttonAgendamento.Enabled = false;
            if (backup != null)
                if (txtDestinoCopia != null)
                    await backup.IniciaBackup();
                else
                {
                    MessageBox.Show("Pasta de Destino não configurada!");
                    return;
                }
            else
            {
                if (txtDestinoCopia != null)
                {
                    configurador.SalvaConfiguracoes(configuracoes);
                    backup = new FazBackup(configuracoes, this);
                    await backup.IniciaBackup();
                }else
                {
                    MessageBox.Show("Pasta de Destino não configurada!");
                    return;
                }
            }
        }

        private void Button1_Click_1(object sender, EventArgs e)
        {
            ConfigBase config = new ConfigBase();
            config.form1 = this;
            configurador.SalvaConfiguracoes(configuracoes);
            if (configuracoes == null)
            {
                config.Show();
            }
            else
            {
                
                config.Show();
            }
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        public void Configura(string _dump, string _ip, string _porta,string _senha,string _usuario,string _base)
        {
            configuracoes.CaminhoPostgre = _dump;
            configuracoes.IpHost = _ip;
            configuracoes.Porta = _porta;
            configuracoes.SenhaBanco = _senha;
            configuracoes.UserBanco = _usuario;
            configuracoes.NomeBase = _base;
            

        }

        private async void Button2_Click_1(object sender, EventArgs e)
        {
            btnFazBackup.Enabled = false;
            btnSalvar.Enabled = false;
            buttonRestaurar.Enabled = false;
            buttonConfigurarBase.Enabled = false;
            buttonAgendamento.Enabled = false;
            restaurador.Restaurar();
            btnFazBackup.Enabled = true;
            btnSalvar.Enabled = true;
            buttonRestaurar.Enabled = true;
            buttonConfigurarBase.Enabled = true;
            buttonAgendamento.Enabled = true;
        }

        private void ButtonAgendamento_Click(object sender, EventArgs e)
        {
            FormAgendaBackup agendar = new FormAgendaBackup();
            agendar.Show();
        }
    }
}
